WITH WeeklySales AS (
    SELECT
        EXTRACT(WEEK FROM r.rental_date) AS WeekNumber,
        SUM(p.amount) AS SalesAmount
    FROM
        rental r
    JOIN payment p ON r.rental_id = p.rental_id
    WHERE
        EXTRACT(WEEK FROM r.rental_date) IN (49, 50, 51)
    GROUP BY
        WeekNumber
),
CenteredAvg AS (
    SELECT
        WeekNumber,
        SalesAmount,
        AVG(SalesAmount) OVER (ORDER BY WeekNumber ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING) AS CENTERED_3_DAY_AVG
    FROM
        WeeklySales
)
SELECT
    WeekNumber,
    SalesAmount AS CUM_SUM,
    CASE
        WHEN EXTRACT(ISODOW FROM MIN(rental_date)) = 1 THEN AVG(SalesAmount) OVER (ORDER BY WeekNumber ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING)
        WHEN EXTRACT(ISODOW FROM MIN(rental_date)) = 5 THEN AVG(SalesAmount) OVER (ORDER BY WeekNumber ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING)
        ELSE CENTERED_3_DAY_AVG
    END AS CENTERED_3_DAY_AVG
FROM
    CenteredAvg
ORDER BY
    WeekNumber;
